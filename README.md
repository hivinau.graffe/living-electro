# Living Electro Crawler

`Living Electro` is a website that I love. It provides some electro, house, bass and dubstep music.
This service is becoming deprecated. The player does not work all the time or the mobile version has limited functionality.

Therefore, I would like to offer a new version of this service that will run under current architectures.
To do so, I start, through this project, by crawling all the data on this site.

## Contents
1. Features
2. Supported OS & SDK version
3. Installation
3. About

### Features


### Supported OS & SDK version

- IntelliJ 2019.3+
- Python 3.7+

### Installation

Install dependencies:
```shell script
pip install -r requirements.txt
```

List available spiders:
```shell script
scrapy list
```

Crawl songs of day:
```shell script
scrapy crawl songs
```

### About

#### Author

Hivinau GRAFFE [hivinau.graffe@hotmail.fr](mailto:hivinau.graffe@hotmail.fr)

#### Contributors

[See CONTRIBUTING](CONTRIBUTING.md)

#### License

This project is released under the MIT license. [See LICENSE](LICENSE) for details.