# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import os
import scrapy
from scrapy.pipelines.images import ImagesPipeline
from scrapy.exceptions import DropItem


class LocalImagesPipeline(ImagesPipeline):

    def __init__(self, store_uri, download_func=None, settings=None):
        super(LocalImagesPipeline, self).__init__(store_uri,
                                             download_func,
                                             settings)

        self.directory_templates = [
            '/full/{0}',
            '/thumbs/small/{0}',
            '/thumbs/medium/{0}'
        ]

    def get_media_requests(self, item, info):
        origin = item['origin']
        image = origin['image']
        if image is not None:
            yield scrapy.Request(image)

    def item_completed(self, results, item, info):
        image_paths = [x['path'] for ok, x in results if ok]
        if not image_paths:
            raise DropItem("Item contains no images")
        directory_filename = image_paths.pop()
        directory, filename = os.path.split(directory_filename)
        item['images'] = [template.format(filename) for template in self.directory_templates]
        return item