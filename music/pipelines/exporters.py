# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.exporters import BaseItemExporter
import firebase_admin
from firebase_admin import credentials, firestore
from google.cloud import storage


class SongItemExporter(BaseItemExporter):

    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings
        return cls(settings)

    def __init__(self, settings):
        super().__init__()

        self._configure_client(settings['FIREBASE_SERVICE_ACCOUNT_KEY'],
                               settings['GCP_API_KEY'],
                               settings['FIREBASE_DATABASE'],
                               settings['GOOGLE_CLOUD_BUCKET_IMAGES'],
                               settings['GOOGLE_CLOUD_BUCKET_TRACKS'])

        self.images_store_directory = settings['IMAGES_STORE']
        self.tracks_store_directory = settings['FILES_STORE']

    def process_item(self, item, spider):
        self._store_item(item)
        self._upload_item_images(item['images'])
        self._upload_item_track(item['track'])

        return item

    def _configure_client(self, service_account_key, gcp_key, database_url, images_bucket_name, tracks_bucket_name):
        cred = credentials.Certificate(service_account_key)
        firebase_app = firebase_admin.initialize_app(cred, {
            'databaseURL': database_url
        })

        self.client = firestore.client(firebase_app)
        storage_client = storage.Client.from_service_account_json(gcp_key)
        self.images_bucket = storage_client.bucket(images_bucket_name)
        self.tracks_bucket = storage_client.bucket(tracks_bucket_name)

    def _store_item(self, item):
        document_reference = self._document_reference(u'songs', item['title'])
        origin = dict(self._get_serialized_fields(item['origin']))
        entry_values = dict(self._get_serialized_fields(item))
        entry_values['origin'] = origin
        document_reference.set(entry_values)

    def _document_reference(self, collectionName, documentName):
        return self.client.collection(collectionName).document(documentName)

    def _upload_item_images(self, images):
        for image in images:
            blob = self.images_bucket.blob(image)
            blob.upload_from_filename('{0}{1}'.format(self.images_store_directory, image))

    def _upload_item_track(self, track):
        blob = self.tracks_bucket.blob(track)
        blob.upload_from_filename('{0}/{1}'.format(self.tracks_store_directory, track))

