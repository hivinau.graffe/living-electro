# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


class BaseUrlProvider(object):

    def __init__(self):
        self.base_url = 'http://www.livingelectro.com'

    def process_item(self, item, spider):
        origin = item['origin']
        image = origin['image']
        if image is not None:
            origin['image'] = '{0}{1}'.format(self.base_url, image)

        source = origin['source']
        if image is not None:
            origin['source'] = '{0}{1}'.format(self.base_url, source)

        item['origin'] = origin

        return item