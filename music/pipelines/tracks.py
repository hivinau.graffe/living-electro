# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import hashlib
import os
import scrapy
from scrapy.pipelines.files import FilesPipeline
from scrapy.exceptions import DropItem
from scrapy.utils.python import to_bytes


class LocalTracksPipeline(FilesPipeline):

    def __init__(self, store_uri, download_func=None, settings=None):
        super(LocalTracksPipeline, self).__init__(store_uri,
                                                  download_func,
                                                  settings)

    def file_path(self, request, response=None, info=None):
        media_guid = hashlib.sha1(to_bytes(request.url)).hexdigest()
        return '{0}.mp3'.format(media_guid)

    def get_media_requests(self, item, info):
        origin = item['origin']
        track = origin['track']
        if track is not None:
            yield scrapy.Request(track)

    def item_completed(self, results, item, info):
        track_paths = [x['path'] for ok, x in results if ok]
        if not track_paths:
            raise DropItem("Item contains no tracks")
        directory_filename = track_paths.pop()
        directory, filename = os.path.split(directory_filename)
        item['track'] = filename
        return item