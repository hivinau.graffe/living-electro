# -*- coding: utf-8 -*-
import scrapy
from music.items.song import Song
from music.items.song_origin import SongOrigin


class Songs(scrapy.Spider):
    name = "songs"
    start_urls = (
        "http://www.livingelectro.com",
    )

    def parse(self, response):
        songs_query_selector = 'div#divMiddleList > div.song_pages.in_view:first-child > div.song_item, ' \
                               'div#divMiddleList > ' \
                               'div.song_pages.in_view:nth-child(2) > div.song_item '

        for element in response.css(songs_query_selector):
            origin = SongOrigin()
            origin['track'] = element.css('div.song_item_playa > div.playa.playerholder::attr(rel)').extract_first()
            origin['source'] = element.css('div.song_item_img > a::attr(href)').extract_first()
            origin['tag'] = element.css('div.song_item_tag > a::text').extract_first()
            origin['image'] = element.css('div.song_item_img > a > img::attr(src)').extract_first()

            song = Song()
            song['title'] = element.css('div.song_item_title > a::text').extract_first()
            song['artist'] = element.css('div.song_item_artist > a::text').extract_first()
            song['origin'] = origin
            yield song

        next_page_url = response.css("li.active ~ li > a::attr(href)").extract_first()
        if next_page_url is not None:
            yield scrapy.Request(response.urljoin(next_page_url))
