# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class SongOrigin(scrapy.Item):
    tag = scrapy.Field()
    image = scrapy.Field()
    track = scrapy.Field()
    source = scrapy.Field()
    pass
